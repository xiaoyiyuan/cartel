'''
This script use k-means clustering to filter out irrelevant tweets 
'''



import pandas as pd
import csv
import numpy as np
from joblib import dump, load
from sklearn.cluster import KMeans
from sklearn import metrics
from sklearn.metrics import pairwise_distances_argmin_min
from sklearn.preprocessing import normalize
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
# from build_entity_networks import get_lop
# from query_places import process_special_cases
import itertools




def filter_embeddings(emb_input, emb_output, unpacked_data):
    '''
    filter pre-trained embeddings to embeddings of entities
    so don't need the full load the full embedding file each time
    '''

    allentities = [x.split(" ") for x in list(set(unpacked_data['entity']))]
    allentities = [item for sublist in allentities for item in sublist]

    checked_count = 0
    with open(emb_input, "r") as readfile:
        with open(emb_output, "w") as writefile:
            writer = csv.writer(writefile)
            for l in readfile:
                if checked_count % 10000 == 0:
                    print("checked", checked_count)
                checked_count += 1
                vec = l.rstrip().split(' ')
                if len(vec) == 2:  # skip first line
                    continue
                if vec[0] in allentities:
                    writer.writerow(vec)

def unpack_data(data, writefile):
    '''
    the original data has entities packed in a dictionary, unpack the formatted
        pickle datas to csv file with columns:
        ['coords', 'tweet_id','lang', 'entities', 'lop']
    '''
    english_stopwords = stopwords.words('english')
    english_stopwords.append('rt')
    spanish_stopwords = stopwords.words('spanish')

    with open(writefile, "w") as csvfile:
        writer = csv.writer(csvfile)
        for i in range(len(data)):
            row = data.iloc[i]
            tweet_id = row['id']
            lang = row['lang']
            x = row['x']  # longitude
            y = row['y']  # latitude
            coordinates = str(x) + "," + str(y)
            entities = row['entities']
            if entities != 'json decoder error' and not np.isnan(x):
                for entity in entities:
                    text = entity.get("matchedText", "")
                    type = "".join(entity.get("freebaseTypes", ""))
                    text, type = process_special_cases(text, type)
                    text = text.lower().replace("#", "")
                    text = " ".join([word for word in text.split(" ") if word not in english_stopwords])
                    text = " ".join([word for word in text.split(" ") if word not in spanish_stopwords])
                    lop = get_lop(type)
                    if (type != '') and (text != '') and (lop != ''):
                        writer.writerow([coordinates, tweet_id, lang, text, lop])

def get_tweet_embeddings(embeddings, unpacked_data):
    '''
    get embeddings for each Tweet by getting the sum of each word's embeddings

    RETURN
    ------
    tweets_entity: a list with unique tweets
    tweets_emb: an array of embeddings for tweets_entity in the same order as tweets_entity
    '''
    embeddings_dict = {embeddings.iloc[i][0]:np.array(embeddings.iloc[i][1:]) for i in range(len(embeddings))}

    # unpacked_data['entity'] has a lot of repeated values
    # we don't need the repeated data for training
    # use unique entities to train, then predict all the rest

    unique_tweets_entities = unpacked_data['entity'].unique()
    tweets_entity = []
    tweets_emb = np.zeros((len(unique_tweets_entities), 300))

    print("Calculating embeddings for tweets...")
    for i in range(len(unique_tweets_entities)):
        tweet_entity = unique_tweets_entities[i]
        word_list = tweet_entity.split(" ")
        tweet_emb = np.zeros(300)
        if len(word_list) == 1: # when the entity is a single word
            # when embeddings not found, return an array of zeros
            tweet_emb = embeddings_dict.get(word_list[0], np.zeros(300))
        if len(word_list) > 1: # when the entity is a phrase, add sum each word's embeddings
            phrase_embeddings = np.zeros((len(word_list), 300))
            for n in range(len(word_list)):
                phrase_embeddings[n] = embeddings_dict.get(word_list[n], np.zeros(300))
            tweet_emb = np.sum(phrase_embeddings, axis=0)
        if np.sum(tweet_emb) != 0:
            tweets_entity.append(tweet_entity)
            tweets_emb[i] = tweet_emb

    # we started the tweets_emb matrix with overflowing rows in the end, remove all of them
    tweets_emb = tweets_emb[~np.all(tweets_emb == 0, axis=1)]
    assert(len(tweets_entity) == len(tweets_emb))

    return tweets_entity, tweets_emb

def kmeans(tweets_emb):
    X = normalize(tweets_emb, 'l1')
    kmeans = KMeans(n_clusters=100, random_state=0).fit(X)
    a, cnts = np.unique(kmeans.labels_, return_counts=True)
    saved_model = dump(kmeans, "../data/relevant_tweets/kmeans.joblib")
    return kmeans


def get_relevant_data(unpacked_data, kmeans, topics_keep, tweets_entity):

    # kmeans clustered unique entities in unpacked_data['entity']
    # now assign all unpacked_data['entity'] a cluster label by finding the same entity text

    labels = kmeans.labels_

    for i in range(len(tweets_entity)):
        if i % 100 == 0:
            print(i)
        if labels[i] in topics_keep:
            entity = tweets_entity[i]
            cluster_label = labels[i]
            unpacked_data.loc[unpacked_data['entity']==entity, 'cluster_label'] = cluster_label

    relevant_data = unpacked_data[~np.isnan(unpacked_data['cluster_label'])]
    relevant_data.to_csv("../data/relevant_tweets/2018_11_relevant.csv", index=False)

    return relevant_data

def add_ms13(unpacked_data, relevant_data):
    '''
    Since ms13 and ms-13 are not in FastText vocab, manually add them as relevant data
    under cluster_label 'e1'
    '''
    relevant_data['cluster_label'] = relevant_data['cluster_label'].astype(int).astype(str)
    ms13 = unpacked_data[unpacked_data['entity']=='ms13']
    ms13.loc[:, 'cluster_label'] = 'e1'
    ms_13 = unpacked_data[unpacked_data['entity']=='ms-13']
    ms_13.loc[:, 'cluster_label'] = 'e1'
    dfs = pd.concat([relevant_data, ms13, ms_13])
    dfs.to_pickle("../data/relevant_tweets/2018_11_relevant.pkl")
    return dfs

def process_special_cases(text, type):
    '''
    A helper function for function location_df
        convert 'cartel' and 'drug cartel' type to organization
    '''
    if text == 'cartel' or text == 'drug cartel':
        return text, 'organization'
    else:
        return text, type

def get_lop(type):
    '''
    Convert the long string of entity types into "l", "o", "p, or "lo", etc
    '''
    dictionary = {"location": "l",
                  "organization": "o",
                  "person": "p"}
    lop_list = [dictionary.get(key) for key in dictionary.keys() if key in type]
    lop = "".join(lop_list)
    return lop


def prep_data(data):
    unpacked_data_file = "../data/relevant_tweets/2018_11_unpacked.csv"
    pretrained_emb_file = "../data/word_embeddings/wiki.en.align.vec" # joint file from both en and es
    filtered_emb_file = "../data/word_embeddings/embeddings_filtered.csv"

    # unpack original .pkl file, write to csv, then read back in as df
    print("Unpacking pickle file...")
    # unpack_data(data, unpacked_data_file)
    unpacked_data = pd.read_csv(unpacked_data_file, header=None)
    unpacked_data.columns = ['coordinates', 'tweet_id', 'lang', 'entity', 'lop']

    # filter original FastText embeddings and filter out OOV embeddings
    # original emb file is very large, so filtering takes time (~1hr)
    # only need to run it once, so it's commented
    print("Filter embeddings...")
    filter_embeddings(pretrained_emb_file, filtered_emb_file, unpacked_data)

    # get embeddings for unique tweets, ready for k-means clustering
    print("Reading embeddings...")
    embeddings = pd.read_csv(filtered_emb_file, header=None)
    print("Getting embeddings for tweets...")
    tweets_entity, tweets_emb = get_tweet_embeddings(embeddings, unpacked_data)

    return unpacked_data, tweets_entity, tweets_emb

def run():
    data = pd.read_pickle("../data/2018_11_formatted.pkl")

    unpacked_data, tweets_entity, tweets_emb = prep_data(data)

    #kmeans_model = kmeans(tweets_emb)
    kmeans_model = load("../data/relevant_tweets/kmeans.joblib")

    # manually select topics to keep, processes not shown here
    topics_keep = [2, 14, 18, 25, 26, 43, 52, 55, 70, 73, 77, 78, 98, 99]

    relevant_data = get_relevant_data(unpacked_data, kmeans_model, topics_keep, tweets_entity)
    relevant_data = add_ms13(unpacked_data, relevant_data)
    relevant_data = pd.read_pickle("../data/relevant_tweets/2018_11_relevant.pkl")


if __name__ == '__main__':
    run()
