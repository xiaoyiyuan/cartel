import pandas as pd
import json
from json import JSONDecodeError
import os

'''
Clean raw csv files and write to pickles:
    Local csv files and raw_pickle files have been deleted
    If need, download from OneDrive or unzip csv_raw.zip and pickle.raw.zip
This script produces the date_formatted.pkl files 
'''

def downsize_data(dir):
    '''
    Delete extra columns from csv files and write to pickles
    '''
    for filename in os.listdir(dir):
        if filename.endswith(".csv"):
            data = pd.read_csv(os.path.join(dir, filename))
            keep_columns = ['id', 'location', 'country', 'state', 'zip', 'x', 'y',
                            'published_at', 'author', 'coords_from', 'retweet_id',
                            'retweet_uid', 'retweet_uname','response_id',
                            'response_author', 'lang', 'text', 'text_razor_result']
            data.drop(columns=[c for c in list(data.columns) if c not in keep_columns],
                      inplace=True)
            data.to_pickle(os.path.join(dir, filename[:7]+'.pkl')) # use year_month

def reformat_data(dir):
    '''
    Reformat column "text_razor_result" from bad json to a list of entity dictionaries
    Create new column "entities" and drop the old one
    '''
    for filename in os.listdir(dir):
        if filename.endswith(".pkl"):
            data = pd.read_pickle(os.path.join(dir, filename))
            data['entities'] = data['text_razor_result'].apply(format_json)
            data.drop(columns="text_razor_result", inplace=True)
            data.to_pickle(os.path.join(dir, filename[:7]+'_formatted'+'.pkl'))


def format_json(bad_json):
    '''

    Parameters
    ----------
    bad_json:
        One row of 'text_razor_result' in bad json format

    Returns
    -------
    A list of dictionaries (each dictionary <--> one named entity).

    '''
    try: # try replacing single quotation with double quotation
        good_json = json.loads(bad_json.replace("\'", "\""))
        dict_list = good_json['response']['entities']
    except JSONDecodeError:
        try: # try to solve the problem of having double quotation inside of single quotation
            string_list = bad_json.split('"')
            for i in range(len(string_list)):
                substring = string_list[i]
                if ": " not in substring:
                    substring = substring.replace("'", "")
                    string_list[i] = "'{}'".format(substring)
            string = "".join(string_list)
            string = string.replace("\'", "\"")
            good_json = json.loads(string)
            dict_list = good_json['response']['entities']
        except: # if there are still problems, give up on parsing (error rate ~0.08%)
            dict_list = 'json decoder error'
    return dict_list


def merged_data(dir):
    '''
    Merge all reformated pickle files to one
    '''
    return None

if __name__ == '__main__':
    dir = "../data/"
    # print("Downsizing csv files and write to pickles...")
    # downsize_data(dir)
    print("Reformating text_razor_result json...")
    reformat_data(dir)


