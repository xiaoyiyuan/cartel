Work flow of clustering Twitter entities (from Molly's data to end result)

1. get Molly's data 

2. clean and reformat Molly's data using script named "clean_rawfiles.py". Output is in pickle format, included in the data file named something like 2018_11_formatted.pkl. I have cleaned all the Twitter data we have. So there should be total 12 pickle files.

3. Using the clean pickle file as input, run script named "get_relevant_tweets_kmeans.py". Other inputs needed to run this is word embeddings, which I have also included in the data I sent you seperately. The trained kmeans model on one month data (2018_11) is also included here. 


All the Python package requirements are included in the requirement.txt file.